# aula-08

# Desafios Estacionamento

## Desafio 1: 
Middleware de Logger para Check-in e Check-out
- Crie um middleware que registre em log as operações de check-in e check-out, incluindo informações como a placa do carro, a hora da operação e o status (entrada ou saída).

## Desafio 2: 
Middleware de Manipulação de Corpo da Solicitação para Check-in
- Implemente um middleware personalizado para validar e manipular os dados do corpo da solicitação durante o check-in, garantindo que informações essenciais, como a placa do carro, sejam fornecidas.

## Desafio 3: 
Roteamento Parametrizado para Check-out
- Implemente uma rota que aceite parâmetros dinâmicos para realizar o check-out, utilizando a placa do carro como parâmetro. Isso permitirá o processamento eficiente de check-outs para carros específicos.

## Desafio 4: 
Proteção contra Check-in Duplicado
- Integre uma validação que evite check-ins duplicados para o mesmo carro na mesma vaga em um curto período de tempo, evitando operações indevidas.

## Desafio 5: 
Verificação de Disponibilidade de Vagas
- Crie um middleware que verifica a disponibilidade de vagas antes de permitir um novo check-in, garantindo que a capacidade do estacionamento não seja excedida.

## Desafio 6: 
Manipulação de Erros Personalizada para Check-out
- Desenvolva middleware(s) que forneçam respostas significativas para diferentes tipos de erros durante o processo de check-out, como tentativa de check-out duplicado ou check-out de um carro não registrado.
