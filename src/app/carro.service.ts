import { Carro } from "../dominio";

export class CarroService {

    carro: Carro
    constructor(carro: Carro) {
        this.carro = carro
    }

    async criarCarro(placa: string, modelo: string, condutor: string) {
        return this.carro.criarCarro(placa, modelo, condutor)
    }

}