import { Carro } from "../dominio/carro"
import { CarroService } from "./carro.service"
import { CarroController } from "./carro.controller"


const carro = new Carro()
const carroService = new CarroService(carro)
const carroController = new CarroController(carroService)

export { carroController }
