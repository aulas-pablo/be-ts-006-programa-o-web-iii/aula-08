import { Request, Response } from "express";
import { CarroService } from "./carro.service";

export class CarroController {
    carroService: CarroService

    constructor(carroService: CarroService) {
        this.carroService = carroService
    }

    async criarCarro(req: Request, res: Response) {
        const resposta = await this.carroService.criarCarro(req.body.placa, req.body.modelo, req.body.condutor)
        res.json(resposta)
    }
}




